# Architecture Analysis

The architecture does not correspond to the level of demand required for a core banking system.
The concept of microservices is compromised, since they all depend on the same database, generating a huge coupling between all functionalities.
It has some event functionality that does not persist throughout the ecosystem and does not have a clear strategy.

## What are the strong points of the Architecture?

Use of third party vendors and funcionalities as KYC, Payments and Card issuing inorder to simplify the solution.

## What are the weaknessess points of the Architecture?

The way they proposal, looks like a distributed monolitich. All micro services depend on the same DB.

## Are there any redflags on the proposal?

1. Analytics and Reporting, using the same production DB, should use at least a Replica DB.
2. The poor event strategy and the DB. 
3. The Analytics and Reporting requires a whole data strategy.

## Which components would you left over? Why?

The events and queue, because it's a powerfull concept but is not well architectet. It needs to be extended to the whole architecture, not only to some use cases.

## Which components would you like to include? Why?

1. Obervability
2. Data Lake
3. Event driven architecture

A banking platform requires a strong approach than the proposal.

## What high-level software patterns can be applied to this architecture?

Software pattersand practices
Clean Architecture for microservices implementation.
DDD to identify the right domains.
Along with the event-driven architecture, use a reactive paradigm.
For some use cases, which are not covered by the event strategy, I would use the SAGA pattern.
CQRS pattern.

## Can a Kappa architecture style be suitable for this problem?

Not sure what is the problem mentioned is the question, but yes, it can be used for enable a big data strategy.

## Is this propoosed architecture event-driven?

No

## Which non-fuunctional requirements are not addressed?

1. Obervability
2. Data strategy 
3. Elasticity 
4. Resilient
5. Eficiency

### How would you address them?

1. Observability: Using AWS tools ecosystem to implement it, such as CloudWatch, combined with third-party strategies such as Prometheus or Dynatrace.
2. Data Strategy: There is no trace of a consistent data strategy in the proposal, data is one of the more important assets of the organization and is necessary implement a data-driven business strategy, this strategyshould be not only for descriptive analytics and reporting, but also predictive and advanced analytics, driving ML and AI models.
3. Elasticity: Microservices will be deployed with Kubernetes, allowing for elasticity, as well as implementing a horizontal scaling strategy for the most critical components.
4. Resilient: The main services will have a high availability strategy, additionally the microservices will be decoupled, and with an event-driven architecture will allow to increase fault tolerance.
5. Efficiency: Depending on the use case, some of the microservices will use Spot instances. Priority will be given to a serverless approach, including lambdas for some operations.

## Any other comments?

In addition to the above, the use of SQL and No-SQL databases is also evaluated, depending on the use case.
It would also implement an information caching strategy like Redis, at the level of data most used by microservices, such as account statements, among others.